import {
    Component, ViewChild, AfterViewInit, OnInit, OnDestroy, ElementRef, NgZone
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { Map } from 'mapbox-gl';
import { MapService } from '../services/map.service';

import { ScatterplotLayer } from '@deck.gl/layers';
import { MapboxLayer } from '@deck.gl/mapbox';
import { TripsLayer } from '@deck.gl/geo-layers';

declare var deck: any;

@Component({
    selector: 'app-vehicle',
    templateUrl: './vehicle.component.html',
    styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit, OnDestroy, AfterViewInit {

    // configure
    public URL = 'http://localhost:4200/ng-deck-gl/';

    public jsonFile = this.URL + 'assets/trip/date-tanggal.json';
    public typeDateTime = 'DATETIME';

    private map$: Subscription;
    private tLayer: any;
    private animationFrame: number;
    public inputSpeed = 1;
    public startTimeDisplay = '';
    public currentTimeDisplay = '';
    public endTimeDisplay = '';
    public currentTimeSaved = 0;
    public valueX: any;
    public nilaiNormal = 1000;
    public nMin = 0;
    public nMax = 0;
    public oMin = 0;
    public oMax = 0;
    public speedCustom = 0;
    public speedCalculateDateTime = 60;
    public speedCalculateDate = 100000;
    public totalStep = 0;
    public trailLengthUI = 1;
    public trailCalculateDateTime = 60;
    public trailCalculateDate = 100000;
    public widthMinPixelsUI = 1;
    public pMIn = 0;
    public pMax = 100;
    public percentStep = 0;
    public step = 1;

    constructor(
        private zone: NgZone,
        private http: HttpClient,
        private mapSvc: MapService,
        private elementRef: ElementRef
    ) { }

    public ngOnInit(): void {

        this.mapSvc.map.subscribe(map => {
            map.flyTo({
                // center: { lng: -74.0123409459859, lat: 40.704499769452724 },
                // 106.803, -6.3542
                center: { lng: 106.803, lat: -6.3542 },
                zoom: 12,
                pitch: 0,
            });
            // this.addMapboxBuildingsLayer(map);
            this.addtLayer(map)
                .then(() => {
                    this.animate();
                })
                .catch(ex => console.error(ex));
        });


    }

    public ngAfterViewInit(): void {
        this.elementRef.nativeElement.querySelector('#progress')
        .addEventListener('input', this.progressChange.bind(this));

        this.elementRef.nativeElement.querySelector('#trailLengt')
        .addEventListener('input', this.trailLengtChange.bind(this));

        this.elementRef.nativeElement.querySelector('#widthpixel')
        .addEventListener('input', this.widthpixelChange.bind(this));

        // this.elementRef.nativeElement.querySelector('#speed')
        // .addEventListener('input', this.speedChange.bind(this));

    }

    // tslint:disable-next-line: typedef
    // public changeTripLayer (
    //     currentTime: number,
    //     trailLength: number,
    //     widthMinPixels: number ) {
    //     this.tLayer.setProps({
    //         currentTime,
    //         trailLength,
    //         widthMinPixels
    //     });
    // }

    public progressChange(event: any): void {

        // dari percent to step
        this.step = this.percentStep / 100 * this.totalStep;

        // dari step ke currentTime (nilai normal)
        this.currentTimeSaved   = this.nMin + this.speedCustom * this.step;
        const currentTime       = this.currentTimeSaved;

        // trailLength
        const trailLength = (this.typeDateTime === 'DATETIME') ?
            this.trailLengthUI * this.trailCalculateDateTime :
            this.trailLengthUI * this.trailCalculateDate ;

        // widthPixel
        const widthMinPixels = this.widthMinPixelsUI;

        // dapatkan nilai original timestamp
        const originalTime = currentTime + this.oMin;

        // convert nilai original (timestamp to datetime)
        this.currentTimeDisplay = this.timeConverter(originalTime).toString();

        // set to triplayer
        this.tLayer.setProps({
            currentTime,
            trailLength,
            widthMinPixels
        });
    }

    public trailLengtChange(event: any): void {
        const trailLength = (this.typeDateTime === 'DATETIME') ?
            this.trailLengthUI * this.trailCalculateDateTime :
            this.trailLengthUI * this.trailCalculateDate ;
        this.tLayer.setProps({
            trailLength
        });
    }

    public widthpixelChange(event: any): void {
        const widthMinPixels = this.widthMinPixelsUI;
        this.tLayer.setProps({
            widthMinPixels
        });
    }

    public ngOnDestroy(): void {
        if (!!this.animationFrame) {
            cancelAnimationFrame(this.animationFrame);
        }
        this.mapSvc.map.subscribe(map => {
            map.removeLayer('buildings')
                .removeLayer('3d-buildings')
                .removeLayer('trips');
        });
    }

    private addDeckBuildingLayer(map: Map): void {
        const layer = new deck.MapboxLayer({
            id: 'buildings',
            type: deck.PolygonLayer,
            data: './assets/buildings.json',
            extruded: true,
            wireframe: false,
            opacity: 0.5,
            getPolygon: f => f.polygon,
            getElevation: f => f.height,
            getFillColor: [74, 80, 87]
        });
        map.addLayer(layer);
    }

    private addMapboxBuildingsLayer(map: Map): void {
        const labelLayer = map.getStyle().layers.find(
            layer => layer.type === 'symbol' && !!layer.layout['text-field']
        );
        const labelLayerId = labelLayer.id;
        map.addLayer({
            id: '3d-buildings',
            source: 'composite',
            'source-layer': 'building',
            filter: ['==', 'extrude', 'true'],
            type: 'fill-extrusion',
            minzoom: 13,
            paint: {
                'fill-extrusion-color': '#aaa',
                'fill-extrusion-height': ['get', 'height'],
                'fill-extrusion-base': ['get', 'min_height'],
                'fill-extrusion-opacity': .6
            }
        }, labelLayerId);
    }

    private async addtLayer(map: Map): Promise<void> {

        // sample data date
        // sample data datetime

        const dataJson = this.jsonFile;

        const trips = await this.http.get<any>(dataJson).toPromise();

        const tripsData = await this.rowToJson(trips); // output normal

        console.log('original data: ', trips);
        console.log('converted data: ', tripsData);

        this.tLayer = new MapboxLayer({
            id: 'trips',
            type: TripsLayer,
            data: tripsData, // 1rb an
            getPath: d => d.path,
            getTimestamps: d => d.timestamps,
            getColor: d => (d.properties.total_people >= 50
            ? [253, 128, 93] : [23, 184, 190]),
            opacity: 1,
            widthMinPixels: this.widthMinPixelsUI,
            rounded: true,
            trailLength: 200,
            currentTime: 0, // 180,
            pickable: true,
            onHover: ({object, x, y}) => {
                // console.log(object);
                const el = document.getElementById('tooltip');
                if (object) {
                    el.innerHTML = `
                        <div>origin: ${object.properties.origin}</div>
                        <div>dest: ${object.properties.dest}</div>
                        <div>
                        total_people: ${object.properties.total_people}
                        </div>
                        <div>Timestamp:<span style="word-wrap: break-word;">
                         ${object.properties.allTimestamp}</span></div>
                    `;
                    el.style.display = 'block';
                    el.style.left = x + 'px';
                    el.style.top = y + 'px';
                } else {
                    el.style.display = 'none';
                }
            },
        });
        map.addLayer(this.tLayer);

        // const match =
        // // tslint:disable-next-line: max-line-length
        // '2020-01-01 06:01:01'
        // .match(/^(\d+)-(\d+)-(\d+) (\d+)\:(\d+)\:(\d+)$/);

        // const matchD =
        // // tslint:disable-next-line: max-line-length
        // '2020-01-01'.match(/^(\d+)-(\d+)-(\d+)$/);

        // const date =
        // // tslint:disable-next-line: max-line-length
        // // new Date(match[1], match[2] - 1,
        // match[3], match[4], match[5], match[6]);
        // new Date(matchD[1], matchD[2] - 1, matchD[3]);

        // const dateT =
        // // tslint:disable-next-line: max-line-length
        // new Date(match[1], match[2] - 1, match[3],
        // match[4], match[5], match[6]);
        // //new Date(match[1], match[2] - 1, match[3]);

        // console.log('date: ', date.getTime() / 1000);
        // console.log('time: ', dateT.getTime() / 1000);

    }

    // tslint:disable-next-line: typedef
    // fungsu dari row ke json menjadi data normal
    public async rowToJson( responsedata: any ) {
        const groupBy = (keys) => (array) =>
        array.reduce((objectsByKeyValue, obj: any) => {
            const value = keys.map((key) => obj[key]).join('-');
            objectsByKeyValue[value] =
            (objectsByKeyValue[value] || []).concat(obj);
            return objectsByKeyValue;
        }, {});

        // const groupByBrandAndYear = groupBy(["origin","dest"]);
        const groupByBrandAndYear = groupBy(['origin', 'dest']);

        // dapatkan nilai paling besar
        const allTimestamp = []; // all timestamp[1,5M..,1,5M..]
        for (const [key, values] of
            Object.entries(groupByBrandAndYear(responsedata))) {
            this.valueX = values;
            let i = 0;
            this.valueX.forEach(element => {
                allTimestamp.push([parseFloat((element.timestamp).toString())]);
                i++;
            });
        }
        this.oMin = Math.min.apply(null, allTimestamp); // 4
        this.oMax = Math.max.apply(null, allTimestamp); // 4
        console.log('oMin:', this.oMin, 'oMax:', this.oMax);

        // dapatkan format json
        const json = [];
        const allTimestampIsConvert = []; // all timestamp
        for (const [key, values] of
            Object.entries(groupByBrandAndYear(responsedata))) {

            this.valueX = values;

            let i = 0;
            let properti: any;
            const pathdata = [];
            const timestampsdata = [];
            this.valueX.forEach(element => {

                if (i === 0) {
                    properti = {
                        kecamatan: element.kecamatan,
                        origin: element.origin,
                        dest: element.dest,
                        total_people: element.total_people, // harus di sum
                        allTimestamp: allTimestamp
                    };
                }

                pathdata
                .push([parseFloat(element.longitude),
                    parseFloat(element.latitude)]);

                // normaliasi data
                allTimestampIsConvert
                .push([parseFloat(((element.timestamp -
                    this.oMin) + 0)
                    .toString())]);

                // normaliasi data
                timestampsdata
                .push(parseFloat(((element.timestamp -
                    this.oMin) + 0)
                    .toString()));

                // allTimestampIsConvert
                // .push([parseFloat((element.timestamp /
                //     this.oMax * this.nilaiNormal)
                //     .toString())]);

                // timestampsdata
                // .push(parseFloat((element.timestamp /
                //     this.oMax * this.nilaiNormal)
                //     .toString()));

                i++;
            });

            json.push({
                properties: properti,
                // vendor: 0,
                path: pathdata,
                timestamps: timestampsdata // 1rb - 2rb
            });
        }

        // nilai max normal
        this.nMax = Math.max.apply(null, allTimestampIsConvert);

        // nilai min normal
        this.nMin = Math.min.apply(null, allTimestampIsConvert);
        console.log('nMin: ', this.nMin, 'nMax: ', this.nMax);

        // console.log(json);
        return json;
    }

    private animate(): void {

        // nilai min dari timestamp
        const nilaiMin = this.nMin;

        // nilai max dari timestamp
        const nilaiMax = this.nMax;

        // configure
        let trailLength = 0;
        let widthMinPixels = 0;
        if ( this.typeDateTime === 'DATETIME' ) { // 60

            this.speedCustom = (this.inputSpeed / 100) *
                this.speedCalculateDateTime; // speed

            trailLength = this.trailLengthUI *
             this.trailCalculateDateTime;  // trail

            widthMinPixels = this.widthMinPixelsUI; // width

        } else { // 100.000

            this.speedCustom = (this.inputSpeed / 100) *
             this.speedCalculateDate; // speed

            trailLength = this.trailLengthUI *
             this.trailCalculateDate; // trail

            widthMinPixels = this.widthMinPixelsUI; // trail

        }

        // total step
        this.totalStep = (this.nMax - this.nMin ) / this.speedCustom;

        // jika currentTime = 0 or currentTime >= nilaiMax
        if ( this.currentTimeSaved === 0 ||
            this.currentTimeSaved >= nilaiMax ) {

            // time ulang dari awal
            this.currentTimeSaved = nilaiMin + this.speedCustom;

            // hitung step mulai dari 1
            this.step = 1;

        } else {

            // time akumulasi ++ speed custom
            this.currentTimeSaved = this.currentTimeSaved + this.speedCustom;

            // step akumulasi ++
            this.step = this.step + 1;
        }

        // set currentTime
        const currentTime   = this.currentTimeSaved;

        // dapatkan nilai original timestamp
        const originalTime = currentTime + this.oMin;

        // set from step to percent
        this.percentStep    = this.step / this.totalStep * 100;

        // display min timestamp
        this.startTimeDisplay = this.timeConverter(this.oMin).toString();

        // display current timestamp
        this.currentTimeDisplay = this.timeConverter(originalTime).toString();

        // display max timestamp
        this.endTimeDisplay = this.timeConverter(this.oMax).toString();

        // console.log('speedCustom: ', this.speedCustom, 'currentTime: ',
        // currentTime, 'step: ', this.step, 'percent: ', this.percentStep,
        // 'original timestamp: ', originalVal);

        // set to layer tip
        this.tLayer.setProps({
            currentTime,
            trailLength,
            widthMinPixels
        });

        // animasi loop
        this.animationFrame = requestAnimationFrame(this.animate.bind(this));

    }

    public Play(): void {
        this.animate();
    }

    public Stop(): void{
        cancelAnimationFrame(this.animationFrame);
    }

    // tslint:disable-next-line: variable-name
    public timeConverter(UNIX_timestamp: number): string {
        const a = new Date(UNIX_timestamp * 1000);
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        const year = a.getFullYear();
        const month = months[a.getMonth()];
        const date = a.getDate();
        const hour = a.getHours();
        const min = a.getMinutes();
        const sec = a.getSeconds();
        const time = date + ' ' + month + ' ' + year + ' '
        + hour + ':' + min + ':' + sec ;
        return time;
    }

    // private animate(): void {
    //     // nilai min
    //     const nilaiMin = 1591570800 - 100;
    //     // nilai max
    //     const loopLength = 1591570832 + 100; // 1583 + 10;
    //     const speedCustom = 1; // 0.1 - 1x
    //     // nilai speed
    //     const animationSpeed = this.inputSpeed; // nilai speed
    //     // timestamp now
    //     const timestamp = Date.now() / 1000;
    //     // const timestamp = Date.now();
    //     const loopTime = loopLength / animationSpeed;
    //     const currentTime = ((timestamp % loopTime) / loopTime) * loopLength;
    //     console.log('currentTime ', currentTime);
    //     this.currentTimeDisplay = currentTime.toString();
    //     this.tLayer.setProps({ currentTime });
    //     this.animationFrame = requestAnimationFrame(this.animate.bind(this));
    // }

}
