import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Ng5SliderModule } from 'ng5-slider';

// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { AppSharedModule } from 'app-shared';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { RiverComponent } from './river/river.component';
import { PointComponent } from './point/point.component';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        VehicleComponent,
        RiverComponent,
        PointComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        // NgbModule,
        // AppSharedModule,
        AppRoutingModule,
        Ng5SliderModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
