import {
    Component, ViewChild, AfterViewInit, OnInit, OnDestroy, ElementRef, NgZone
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { Map } from 'mapbox-gl';
import { MapService } from '../services/map.service';
// import { ScatterplotLayer } from '@deck.gl/layers';
import { ScatterplotLayer } from '@deck.gl/layers';
import { MapboxLayer } from '@deck.gl/mapbox';
import {DataFilterExtension} from '@deck.gl/extensions';

import { Options } from 'ng5-slider';

declare var deck: any;

@Component({
  selector: 'app-point',
  templateUrl: './point.component.html',
  styleUrls: ['./point.component.scss']
})
export class PointComponent implements OnInit {

    // configure
    public URL = 'http://localhost:4200/ng-deck-gl/';
    public jsonFile = this.URL + 'assets/point-layer.json';

    // development
    // public URL = 'http://localhost:3002/api/ref/';
    // public jsonFile = this.URL + 'deckgl-point';

    private map$: Subscription;
    private tripsLayer: any;
    private animationFrame: number;

    public pointLayer: any;
    public nMin = 0;
    public nMid = 0;
    public nMax = 0;
    public oMin = 0;
    public oMid = 0;
    public oMax = 0;
    public inputSpeed = 1;
    public speedCustom = 0;
    public speedCalculateDateTime = 60;
    public speedCalculateDate = 100000;
    public totalStep = 0;
    public trailLengthUI = 1;
    public trailCalculateDateTime = 60;
    public trailCalculateDate = 100000;
    public widthMinPixelsUI = 1;
    public pMIn = 0;
    public pMax = 100;
    public percentStep = 0;
    public step = 1;

    public valuePercent = 0;
    public highValuePercent = 10;

    public valueNormal = 0;
    public highValueNormal = 0;

    public valueOriginal = 0;
    public highValueOriginal = 0;

    public valueOriginalDisplay = null;
    public highValueOriginalDisplay = null;

    public value = 0;
    public highValue = 10;
    public options: Options = {
        floor: 1,
        ceil: 100,
        step: 0.01,
        enforceStep: false,
        enforceRange: false,
        animate: false
    };

    public logText = '';


    constructor(
        private zone: NgZone,
        private http: HttpClient,
        private mapSvc: MapService,
        private elementRef: ElementRef
        // private plotLayer: ScatterplotLayer
    ) { }

    public ngOnInit(): void {

        this.mapSvc.map.subscribe(map => {
            map.flyTo({
                center: { lng: 115.927734375, lat: -2.986927393334863 },
                zoom: 4,
                pitch: 20,
            });
            this.addPointLayer(map)
                .then(() => {
                    // cancelAnimationFrame(this.animationFrame);
                    this.animate();
                })
                .catch(ex => console.error(ex));
        });

    }

    private async addPointLayer(map: Map): Promise<void> {

        // const {GeoJsonLayer, ScatterplotLayer} = deck;

        const point = await this.http.get<any>(this.jsonFile).toPromise();
        const pointData = await this.rowToJson(point); // output normal
        console.log(point);
        console.log(pointData);
        this.pointLayer = new MapboxLayer({
            id: 'scatter',
            type: ScatterplotLayer,
            source: 'scatter',
            data: pointData,

            // opacity: 0.8,
            // stroked: true,
            filled: true,
            radiusScale: 5,
            radiusMinPixels: 1,
            radiusMaxPixels: 100,
            lineWidthMinPixels: 1,
            getPosition: d => [parseFloat(d.longitude), parseFloat(d.latitude)],
            getRadius: d => Math.sqrt(d.total_people * 1000),
            // getFillColor: d => [255, 140, 0],
            // getLineColor: d => [0, 0, 0],

            getFillColor: d =>
                d.total_people >= 50 ? [200, 0, 40, 150] : [255, 140, 0, 100],

            pickable: true,
            onHover: ({object, x, y}) => {
                console.log(object);
                const el = document.getElementById('tooltip');
                if (object) {
                    el.innerHTML = `
                        <div>provinsi: ${object.provinsi}</div>
                        <div>kecamatan: ${object.kecamatan}</div>
                        <div>kabupaten: ${object.kabupaten}</div>
                        <div>desa: ${object.desa}</div>
                        <div>date: ${object.date}</div>
                        <div>total_people: ${object.total_people}</div>`;
                    el.style.display = 'block';
                    el.style.left = x + 'px';
                    el.style.top = y + 'px';
                } else {
                    el.style.display = 'none';
                }
            },

            // filter animasi
            // getFilterValue: d => d.timestamp, // nilai normal
            // // nilai normal terkecil dan nilai normal 10%
            // filterRange: [this.nMin, this.nMid], // di update oleh animasi
            // extensions: [new DataFilterExtension({filterSize: 1})]

          });

        // kondisi kalo ada data date/datetime
        // kondisi di viz atau dashboard
        this.pointLayer.setProps({
            // filter animasi
            getFilterValue: d => d.timestamp, // nilai normal
            // nilai normal terkecil dan nilai normal 10%
            filterRange: [this.nMin, this.nMid], // di update oleh animasi
            extensions: [new DataFilterExtension({filterSize: 1})]
        });


        map.addLayer(this.pointLayer);

    }

    // tslint:disable-next-line: typedef
    public async rowToJson( responsedata: any ) {

        const allTimestamp = []; // all timestamp[1,5M..,1,5M..]
        // const allId = [];
        responsedata.forEach(element => {

            // dari date/datetime
            const dd = (element.date).match(/^(\d+)-(\d+)-(\d+)$/);
            const y = parseFloat(dd[1]);
            const m = parseFloat(dd[2]);
            const d = parseFloat(dd[3]);
            const date = new Date(y, m - 1, d);

            // convert to timestamp`
            const oTime = date.getTime() / 1000;

            // input ke array [1,5M, 1,5M dst...]
            allTimestamp.push(oTime);

            // allId.push(element.id);

        });

        // nilai original max
        this.oMax = Math.max.apply(null, allTimestamp);

        // nilai original min
        this.oMin = Math.min.apply(null, allTimestamp);

        const allTimestampIsConvert = [];
        const allIdIsConvert = [];
        const data = [];
        responsedata.forEach(element => {

            const dd = (element.date).match(/^(\d+)-(\d+)-(\d+)$/);
            const y = parseFloat(dd[1]);
            const m = parseFloat(dd[2]);
            const d = parseFloat(dd[3]);
            const date = new Date(y, m - 1, d);
            const nTime = date.getTime() / 1000;

            // normaliasi data
            allTimestampIsConvert
            .push(parseFloat(((nTime - this.oMin) + 0).toString()));

            allIdIsConvert
            .push(parseFloat(((element.id - this.oMin) + 0).toString()));

            data.push({
                timestamp: parseFloat(((nTime - this.oMin) + 0) .toString()),
                date: element.date,
                desa: element.desa,
                id: element.id,
                id_desa: element.id_desa,
                id_kab: element.id_kab,
                id_kec: element.id_kec,
                id_prov: element.id_prov,
                kabupaten: element.kabupaten,
                kecamatan: element.kecamatan,
                latitude: element.latitude,
                longitude: element.longitude,
                provinsi: element.provinsi,
                total_people: element.total_people
            });

        });

        // nilai max normal 1,5M => 111111
        this.nMax = Math.max.apply(null, allTimestampIsConvert);

        // nilai min normal 1,5M => 0
        this.nMin = Math.min.apply(null, allTimestampIsConvert);

        // nilai mid dari mid utk start awal ke 10%
        const highValueNormal = this.highValue * this.nMax / 100;
        this.nMid = highValueNormal + this.nMin;

        console.log(
            'oMin:', this.oMin,
            'oMax:', this.oMax);

        console.log(
        'nMin: ', this.nMin,
        'nMid:', this.nMid,
        'nMax: ', this.nMax);

        return data;
    }

    private animate(): void {

        // total step
        // this.totalStep = (this.nMax - this.nMin ) / this.speedCustom;

        // nilai speed
        this.speedCustom = this.inputSpeed / 100;

        // set awal nilai 0 atau 100
        if ( this.value === 0 || this.highValue >= 100 ) {

            // jika di custom slide max nya
            const hValue = this.highValue - this.value;

            // percent ++ speed
            this.value = 0 + this.speedCustom;
            this.highValue = hValue + this.speedCustom;

            // percent di convert ke nilai normal
            this.valueNormal = this.value * this.nMax / 100;
            this.highValueNormal = this.highValue * this.nMax / 100;

            // nilai normal di convert ke nilai original
            this.valueOriginal = this.valueNormal + this.oMin;
            this.highValueOriginal = this.highValueNormal + this.oMin;


        // set selanjutnya
        } else {

            // 1,2, => 99
            this.value = this.value + this.speedCustom;
            this.highValue = this.highValue + this.speedCustom;

            this.valueNormal = this.value * this.nMax / 100;
            this.highValueNormal = this.highValue * this.nMax / 100;

            this.valueOriginal = this.valueNormal + this.oMin;
            this.highValueOriginal = this.highValueNormal + this.oMin;

        }

        this.valueOriginalDisplay =
        this.timeConverter(this.valueOriginal);

        this.highValueOriginalDisplay =
        this.timeConverter(this.highValueOriginal);

        console.log(
            this.speedCustom,
            this.value,
            this.highValue,
            this.valueNormal,
            this.highValueNormal,
            this.valueOriginal,
            this.highValueOriginal
            );


        // console.log(this.valueTimeStamp)

        const filterRange = [this.valueNormal, this.highValueNormal];

        // console.log(this.value, this.highValue);

        // console.log(this.animationFrame);

        // set to layer tip
        this.pointLayer.setProps({
            filterRange
        });

        this.animationFrame = requestAnimationFrame(this.animate.bind(this));
    }

    // tslint:disable-next-line: variable-name
    public timeConverter(UNIX_timestamp: number): string {
        const a = new Date(UNIX_timestamp * 1000);
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        const year = a.getFullYear();
        const month = months[a.getMonth()];
        const date = a.getDate();
        const hour = a.getHours();
        const min = a.getMinutes();
        const sec = a.getSeconds();
        const time = date + ' ' + month + ' ' + year + ' '
        + hour + ':' + min + ':' + sec ;
        // const time = date + ' ' + month + ' ' + year;
        return time;
    }

    public Play(): void {
        this.animate();
    }

    public Stop(): void {
        cancelAnimationFrame(this.animationFrame);
    }


    public onUserChange(changeContext: any): void {
        console.log(changeContext.value, changeContext.highValue);

        this.value = changeContext.value;
        this.highValue = changeContext.highValue;

        this.valueNormal = this.value * this.nMax / 100;
        this.highValueNormal = this.highValue * this.nMax / 100;

        this.valueOriginal = this.valueNormal + this.oMin;
        this.highValueOriginal = this.highValueNormal + this.oMin;

        this.valueOriginalDisplay =
        this.timeConverter(this.valueOriginal);

        this.highValueOriginalDisplay =
        this.timeConverter(this.highValueOriginal);

        const filterRange = [this.valueNormal, this.highValueNormal];

        this.pointLayer.setProps({
            filterRange
        });

    }





}
